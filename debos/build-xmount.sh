#!/bin/sh

# workarround from https://gitlab.com/gitlab-com/support-forum/issues/3732
# This does not work, because it needs to be done during the debos execution,
# therefore, I try to simply mount it manually to /dev
# mkdir -p /tmp/dev
# mount -t devtmpfs none /tmp/dev

# for i in $(seq 0 9); do
  # mknod -m 0660 "/tmp/dev/loop$i" b 7 "$i"
# done

./fake-kvm.sh
debos citar-xmount.yaml